package com.deprem.numberguess

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.io.IOException
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel

class MainActivity : AppCompatActivity() {

    private lateinit var edittextNumber1: EditText
    private lateinit var edittextNumber2: EditText
    private lateinit var button: Button
    private lateinit var textViewResult: TextView
    private lateinit var textViewPrediction: TextView
    private lateinit var tflite: Interpreter
    private lateinit var name: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        val options = Interpreter.Options()
        options.setNumThreads(5)
        options.setUseNNAPI(true)

        tflite = Interpreter(getModelByteBuffer(), options)

        button.setOnClickListener {

            makePrediction(edittextNumber1, edittextNumber2)
        }

    }

    // My model accepts 2 inputs to detect next number and we need to give these 2 numbers in array format

    // Prediction value will be in array format

    private fun makePrediction(editText: EditText, editText2: EditText) {

        val input1 = editText.text.toString()
        val input2 = editText2.text.toString()

        if ((input1.trim() == "") || (input2.trim() == "")) {
            Toast.makeText(this, "please enter numbers", Toast.LENGTH_SHORT).show()
            Log.d("****", input1)
        } else {
            // You need to give output shape to your model as array
            val output =
                Array(1) { FloatArray(1) }

            showCorrectResult(input2)
            tflite.run(createInputArray(input1.toFloat(), input2.toFloat()), output)
            // My model is not very accurate for this reason there might be big differences between correct result and prediction
            textViewPrediction.text = "Predicted result is: " + output[0][0].toString()

        }
    }

    // Initializing all views
    private fun init() {

        edittextNumber1 = findViewById(R.id.editText1)
        edittextNumber2 = findViewById(R.id.editText2)
        button = findViewById(R.id.buttonPredict)
        textViewPrediction = findViewById(R.id.textViewPredection)
        textViewResult = findViewById(R.id.textViewResult)
    }

    // Giving 2 numbers for model to detect next number after these 2 numbers
    private fun createInputArray(number1: Float, number2: Float): FloatArray {

        val inputArray = FloatArray(2)
        inputArray[0] = number1
        inputArray[1] = number2

        return inputArray

    }

    // Just for showing correct result
    private fun showCorrectResult(number2: String) {

        textViewResult.text = "Correct result is: " + (number2.toInt() + 1).toString()

    }

    // Defining deep learning model as TensorflowLite model to use
    @Throws(IOException::class)
    private fun getModelByteBuffer(): MappedByteBuffer {
        val fileDescriptor = assets.openFd("model.tflite")
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }
}
